package com.santander.birra.components;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.core.ReactiveValueOperations;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.santander.birra.configurations.ClimateParameterConfig;
import com.santander.birra.dto.climate.ClimateResponseDto;
import com.santander.birra.model.Province;
import com.santander.birra.service.ProvinceService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;
import reactor.util.retry.Retry;

@Component
public class ClimateCache {

	Logger logger = LoggerFactory.getLogger(ClimateCache.class);

	@Autowired
	private ClimateParameterConfig climateParameterConfig;

	@Autowired
	private ProvinceService provinceService;

	@Autowired
	private ReactiveRedisTemplate<String, ClimateResponseDto> redisTemplate;

	WebClient client;

	/**
	 * Start the process that fills the redis cache with the climate of the
	 * provinces 
	 */
	@Scheduled(initialDelay = 0L, fixedDelayString = "${santander.climate.update-cache}")
	public void initClimateCache() {

		logger.info("start climate cache.");

		/*
		 * get all province
		 */
		Flux<Province> prFlux = provinceService.getAll();

		logger.debug("climate url, host: {}, endpoint: {} ", climateParameterConfig.getHost(),
				climateParameterConfig.getEndPoint());

		/*
		 * start the client to call the weather api
		 */
		client = WebClient.create(climateParameterConfig.getHost());

		/*
		 * call the weather api for each province
		 */
		Flux<ClimateResponseDto> climates = prFlux.parallel().runOn(Schedulers.boundedElastic())
				.flatMap(p -> callClimate(p.getName())).sequential();

		/*
		 * Start redis cache
		 */
		ReactiveValueOperations<String, ClimateResponseDto> cache = redisTemplate.opsForValue();

		Optional<List<ClimateResponseDto>> climatesOptional = climates.collectList().blockOptional();

		/*
		 * if it has climates it saves them in the redis cache
		 */
		if (climatesOptional.isPresent())
			climatesOptional.get().forEach(cl -> {

				logger.info("save in cache: {}", cl.getCityName());

				Mono<Boolean> result = cache.set(cl.getCityName(), cl);

				StepVerifier.create(result).expectNext(true).verifyComplete();
			});

	}

	/**
	 * Call the weather api by province name
	 * 
	 * @param provinceName {@link String}
	 * @return {@link Mono} {@link ClimateResponseDto}
	 */
	private Mono<ClimateResponseDto> callClimate(String provinceName) {

		Map<String, String> parameters = Map.of("city", provinceName, "contry", climateParameterConfig.getContryCode(),
				"key", climateParameterConfig.getKey());

		logger.debug("key for call service of climate: {}", climateParameterConfig.getKey());

		return client.get().uri(climateParameterConfig.getEndPoint(), parameters).retrieve()
				.bodyToMono(ClimateResponseDto.class).retryWhen(Retry.fixedDelay(3, Duration.ofSeconds(5)))
				.delaySubscription(Duration.ofSeconds(10));
	}

}
