package com.santander.birra.components;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.santander.birra.dto.notification.NotificationDto;
import com.santander.birra.service.MeetupService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

@Component
public class NotificationPublisher {

	Logger logger = LoggerFactory.getLogger(NotificationPublisher.class);

	@Autowired
	private Sinks.Many<NotificationDto> sink;

	@Autowired
	private MeetupService meetupService;

	/**
	 * update the notifications to send with new meetup using the method
	 * {@link MeetupService#getByNotifiedIsFalse()}
	 */
	@Scheduled(fixedRate = 15000)
	public void publish() {

		meetupService.getByNotifiedIsFalse().subscribe(m -> {

			logger.info("notify new meetup: {}", m.getName());

			/*
			 * push new notifications
			 */
			sink.tryEmitNext(new NotificationDto(m.getName(), "Hay un nueva Meetup", m.getProvince().getId()));

			m.setNotified(true);

			/*
			 * save in the db that they were already notified
			 */
			meetupService.save(Mono.just(m)).subscribe();
		});
	}

	@Bean
	public Flux<NotificationDto> flux(Sinks.Many<NotificationDto> sink) {
		return sink.asFlux();
	}

	@Bean
	public Sinks.Many<NotificationDto> sink() {
		return Sinks.many().replay().latest();
	}

}
