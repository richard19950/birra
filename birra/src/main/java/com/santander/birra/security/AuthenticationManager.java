package com.santander.birra.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import reactor.core.publisher.Mono;

@Component
public class AuthenticationManager implements ReactiveAuthenticationManager {

	@Autowired
	private JWTUtil jwtUtil;

	@Override
	public Mono<Authentication> authenticate(Authentication authentication) {
		
		String authToken = authentication.getCredentials().toString();

		if (!jwtUtil.validateToken(authToken))
			return Mono.empty();

		Claims claims = jwtUtil.getAllClaimsFromToken(authToken);

		String rolesMap = claims.get("role", String.class);

		List<GrantedAuthority> authorities = List.of(new SimpleGrantedAuthority(rolesMap));

		return Mono.just(new UsernamePasswordAuthenticationToken(claims.get("user"), null, authorities));
	}

}
