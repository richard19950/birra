package com.santander.birra.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.santander.birra.model.enums.Roles;

import io.swagger.v3.oas.annotations.Hidden;

public class UserDto {

	@Hidden
	private String id;

	@NotBlank(message = "userName cannot be empty")
	private String userName;

	@NotBlank(message = "pass cannot be empty")
	private String pass;

	@NotNull(message = "role cannot be empty")
	private Roles role;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public Roles getRole() {
		return role;
	}

	public void setRole(Roles role) {
		this.role = role;
	}

}
