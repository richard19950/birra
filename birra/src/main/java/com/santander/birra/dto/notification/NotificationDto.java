package com.santander.birra.dto.notification;

public class NotificationDto {

	private String meetupName;

	private String message;

	private String provinceId;

	public NotificationDto(String meetupName, String message, String provinceId) {
		super();
		this.meetupName = meetupName;
		this.message = message;
		this.provinceId = provinceId;
	}

	public String getMeetupName() {
		return meetupName;
	}

	public void setMeetupName(String meetupName) {
		this.meetupName = meetupName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

}
