package com.santander.birra.dto;

import javax.validation.constraints.NotBlank;

import io.swagger.v3.oas.annotations.Hidden;

public class UserMeetupDto {

	@Hidden
	private String id;

	@NotBlank(message = "meetupId cannot be empty")
	private String meetupId;

	private boolean toGo;

	public String getMeetupId() {
		return meetupId;
	}

	public void setMeetupId(String meetupId) {
		this.meetupId = meetupId;
	}

	public boolean isToGo() {
		return toGo;
	}

	public void setToGo(boolean toGo) {
		this.toGo = toGo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
