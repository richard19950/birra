package com.santander.birra.dto.error;

public class Error {

	private String messenger;

	public Error(String messenger) {
		super();
		this.messenger = messenger;
	}

	public String getMessenger() {
		return messenger;
	}

	public void setMessenger(String messenger) {
		this.messenger = messenger;
	}

}
