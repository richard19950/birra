package com.santander.birra.dto;

public class MeetupProvisionDto {

	private Integer boxesAmount;

	private String nameMeetup;

	private Double temp;

	private Long peopleAmount;

	public Integer getBoxesAmount() {
		return boxesAmount;
	}

	public void setBoxesAmount(Integer boxesAmount) {
		this.boxesAmount = boxesAmount;
	}

	public String getNameMeetup() {
		return nameMeetup;
	}

	public void setNameMeetup(String nameMeetup) {
		this.nameMeetup = nameMeetup;
	}

	public Long getPeopleAmount() {
		return peopleAmount;
	}

	public void setPeopleAmount(Long peopleAmount) {
		this.peopleAmount = peopleAmount;
	}

	public Double getTemp() {
		return temp;
	}

	public void setTemp(Double temp) {
		this.temp = temp;
	}

}
