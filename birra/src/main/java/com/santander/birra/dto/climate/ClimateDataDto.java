package com.santander.birra.dto.climate;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

public class ClimateDataDto {

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@JsonProperty("valid_date")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate validDate;

	@JsonProperty("low_temp")
	private Double lowTemp;

	@JsonProperty("max_temp")
	private Double maxTemp;

	public ClimateDataDto() {
		super();
	}

	public ClimateDataDto(LocalDate validDate, Double lowTemp, Double maxTemp) {
		super();
		this.validDate = validDate;
		this.lowTemp = lowTemp;
		this.maxTemp = maxTemp;
	}

	public LocalDate getValidDate() {
		return validDate;
	}

	public void setValidDate(LocalDate validDate) {
		this.validDate = validDate;
	}

	public Double getLowTemp() {
		return lowTemp;
	}

	public void setLowTemp(Double lowTemp) {
		this.lowTemp = lowTemp;
	}

	public Double getMaxTemp() {
		return maxTemp;
	}

	public void setMaxTemp(Double maxTemp) {
		this.maxTemp = maxTemp;
	}

}
