package com.santander.birra.dto.auth;

import javax.validation.constraints.NotBlank;

public class AuthRequestDto {

	@NotBlank(message = "user cannot be empty")
	private String user;

	@NotBlank(message = "pass cannot be empty")
	private String pass;

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

}
