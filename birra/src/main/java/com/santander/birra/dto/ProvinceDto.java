package com.santander.birra.dto;

import javax.validation.constraints.NotBlank;

import io.swagger.v3.oas.annotations.Hidden;

public class ProvinceDto {

	@Hidden
	private String id;

	@NotBlank(message = "name cannot be empty")
	private String name;

	private String description;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
