package com.santander.birra.dto.climate;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClimateResponseDto {

	@JsonProperty("city_name")
	private String cityName;

	@JsonProperty("state_code")
	private String stateCode;

	private List<ClimateDataDto> data;

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public List<ClimateDataDto> getData() {
		return data;
	}

	public void setData(List<ClimateDataDto> data) {
		this.data = data;
	}

}
