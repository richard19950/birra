package com.santander.birra.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.santander.birra.dto.climate.ClimateResponseDto;

/**
 * 
 * Configuration class to raise the redis bean with the data that is needed to
 * save the weather
 * 
 * @author ricky
 *
 */
@Configuration
public class RedisConfig {

	@Bean
	public ReactiveRedisTemplate<String, ClimateResponseDto> reactiveRedisTemplate(
			ReactiveRedisConnectionFactory factory) {

		StringRedisSerializer keySerializer = new StringRedisSerializer();

		Jackson2JsonRedisSerializer<ClimateResponseDto> valueSerializer = new Jackson2JsonRedisSerializer<>(
				ClimateResponseDto.class);

		RedisSerializationContext.RedisSerializationContextBuilder<String, ClimateResponseDto> builder = RedisSerializationContext
				.newSerializationContext(keySerializer);

		RedisSerializationContext<String, ClimateResponseDto> context = builder.value(valueSerializer).build();

		return new ReactiveRedisTemplate<>(factory, context);
	}

}
