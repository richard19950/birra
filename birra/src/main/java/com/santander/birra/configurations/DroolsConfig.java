package com.santander.birra.configurations;

import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.runtime.KieContainer;
import org.kie.internal.io.ResourceFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class to raise the drools file with the business rules
 * 
 * @author ricky
 *
 */
@Configuration
@ComponentScan("com.santander.birra.service")
public class DroolsConfig {

	private static final String DRL_FILE = "drl/BEER_BOX_AMOUNT.drl";

	@Bean
	public KieContainer kieContainer() {

		KieServices kieServices = KieServices.Factory.get();

		KieFileSystem kieFileSystem = kieServices.newKieFileSystem();

		kieFileSystem.write(ResourceFactory.newClassPathResource(DRL_FILE));

		KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem);

		kieBuilder.buildAll();

		KieModule kieModule = kieBuilder.getKieModule();

		return kieServices.newKieContainer(kieModule.getReleaseId());
	}

}
