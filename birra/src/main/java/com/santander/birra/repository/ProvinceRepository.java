package com.santander.birra.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.santander.birra.model.Province;

import reactor.core.publisher.Flux;

@Repository
public interface ProvinceRepository extends ReactiveMongoRepository<Province, String> {

	Flux<Province> findByName(String name);
}
