package com.santander.birra.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.santander.birra.model.Meetup;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface MeetupRepository extends  ReactiveMongoRepository<Meetup, String> {

	public Mono<Meetup> findByIdAndProvinceIsNotNull(String id);
	
	public Flux<Meetup> findByNotifiedIsFalseOrNotifiedIsNull();
	
}
