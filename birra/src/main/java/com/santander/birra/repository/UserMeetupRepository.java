package com.santander.birra.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.santander.birra.model.UserMeetup;

import reactor.core.publisher.Mono;

@Repository
public interface UserMeetupRepository extends ReactiveMongoRepository<UserMeetup, String> {

	Mono<Long> countByMeetupIdAndToGoTrue(String meetupId);

	Mono<Boolean> existsByMeetupIdAndUserId(String meetupId, String userId);

}
