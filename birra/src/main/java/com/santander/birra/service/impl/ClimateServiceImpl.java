package com.santander.birra.service.impl;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.core.ReactiveValueOperations;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.santander.birra.dto.climate.ClimateDataDto;
import com.santander.birra.dto.climate.ClimateResponseDto;
import com.santander.birra.exception.ErrorConstant;
import com.santander.birra.exception.ServiceException;
import com.santander.birra.service.ClimateService;

import reactor.core.publisher.Mono;

@Service
public class ClimateServiceImpl implements ClimateService {

	Logger logger = LoggerFactory.getLogger(ClimateServiceImpl.class);

	@Autowired
	private ReactiveRedisTemplate<String, ClimateResponseDto> redisTemplate;

	@Override
	public Mono<ClimateDataDto> getClimateByDateAndProvince(LocalDate date, String province) {

		logger.info("get province climate: {}", province);

		if (date.isBefore(LocalDate.now()))
			return Mono.error(new ServiceException(ErrorConstant.THE_MEETUP_IS_OLD, HttpStatus.BAD_REQUEST));

		ReactiveValueOperations<String, ClimateResponseDto> cache = redisTemplate.opsForValue();

		return cache.get(province)
				.map(c -> c.getData().stream().filter(d -> d.getValidDate().equals(date)).findFirst().get())
				.onErrorResume(
						e -> Mono.error(new ServiceException(ErrorConstant.CLIMATE_NOT_FOUND, HttpStatus.NOT_FOUND)));
	}

}
