package com.santander.birra.service;

import java.time.LocalDate;

import com.santander.birra.dto.climate.ClimateDataDto;

import reactor.core.publisher.Mono;

public interface ClimateService {

	/**
	 * get the climate for the day and the province if it does not return an
	 * {@link Mono#error(Throwable)}
	 * 
	 * @param date     {@link LocalDate}
	 * @param province {@link String}
	 * @return {@link Mono} {@link ClimateDataDto}
	 */
	public Mono<ClimateDataDto> getClimateByDateAndProvince(LocalDate date, String province);

}
