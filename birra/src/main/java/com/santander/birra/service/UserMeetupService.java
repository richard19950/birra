package com.santander.birra.service;

import com.santander.birra.model.Meetup;
import com.santander.birra.model.UserMeetup;

import reactor.core.publisher.Mono;

public interface UserMeetupService {

	/**
	 * Save {@link UserMeetup} in db, if already exists returns
	 * {@link Mono#error(java.util.function.Supplier)}
	 * 
	 * @param userMeetupMono {@link Mono} {@link UserMeetup}
	 * @return {@link Mono} {@link UserMeetup}
	 */
	public Mono<UserMeetup> save(Mono<UserMeetup> userMeetupMono);

	/**
	 * Get count {@link UserMeetup} of {@link Meetup}, if is empty return 0
	 * 
	 * @param meetupId {@link String}
	 * @return {@link Mono} {@link Long}
	 */
	public Mono<Long> getCountConfirmedPeopleByMeetupId(String meetupId);

}
