package com.santander.birra.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.santander.birra.dto.auth.AuthRequestDto;
import com.santander.birra.dto.auth.AuthResponseDto;
import com.santander.birra.exception.ErrorConstant;
import com.santander.birra.exception.ServiceException;
import com.santander.birra.security.JWTUtil;
import com.santander.birra.service.AuthenticationService;
import com.santander.birra.service.UserService;

import reactor.core.publisher.Mono;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

	@Autowired
	private JWTUtil jwtUtil;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	private UserService userService;

	@Override
	public Mono<AuthResponseDto> auth(AuthRequestDto authRequestDto) {

		return userService.getByUsername(authRequestDto.getUser())
				.switchIfEmpty(Mono
						.error(new ServiceException(ErrorConstant.INCORRECT_USER_OR_PASSWORD, HttpStatus.UNAUTHORIZED)))
				.flatMap(a -> {

					if (passwordEncoder.matches(authRequestDto.getPass(), a.getPass()))
						return Mono.just(new AuthResponseDto(jwtUtil.generateToken(a)));

					return Mono.error(
							new ServiceException(ErrorConstant.INCORRECT_USER_OR_PASSWORD, HttpStatus.UNAUTHORIZED));
				});

	}

}
