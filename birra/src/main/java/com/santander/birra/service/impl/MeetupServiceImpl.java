package com.santander.birra.service.impl;

import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.santander.birra.dto.MeetupProvisionDto;
import com.santander.birra.dto.climate.ClimateDataDto;
import com.santander.birra.exception.ErrorConstant;
import com.santander.birra.exception.ServiceException;
import com.santander.birra.model.Meetup;
import com.santander.birra.repository.MeetupRepository;
import com.santander.birra.service.ClimateService;
import com.santander.birra.service.MeetupService;
import com.santander.birra.service.UserMeetupService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class MeetupServiceImpl implements MeetupService {

	Logger logger = LoggerFactory.getLogger(MeetupServiceImpl.class);

	@Autowired
	private MeetupRepository meetupRepository;

	@Autowired
	private ClimateService climateService;

	@Autowired
	private UserMeetupService userMeetupService;

	@Autowired
	private KieContainer kieContainer;

	@Override
	public Mono<Meetup> save(Mono<Meetup> meetup) {

		logger.info("save meetup");

		return meetupRepository.saveAll(meetup).last();
	}

	@Override
	public Mono<Meetup> getById(String meetupId) {

		logger.info("get meetup: {}", meetupId);

		return meetupRepository.findById(meetupId)
				.switchIfEmpty(Mono.error(new ServiceException(ErrorConstant.MEETAUP_NOT_FOUND, HttpStatus.NOT_FOUND)));
	}

	@Override
	public Mono<ClimateDataDto> getClimateByMeetupId(String meetupId) {

		logger.info("get the meetup climate: {}", meetupId);

		return getById(meetupId).flatMap(
				meetup -> climateService.getClimateByDateAndProvince(meetup.getDate(), meetup.getProvince().getName()))
				.switchIfEmpty(Mono
						.error(new ServiceException(ErrorConstant.UNEXPECTED_ERROR, HttpStatus.INTERNAL_SERVER_ERROR)));

	}

	@Override
	public Mono<MeetupProvisionDto> getMeetupProvisionDtoByMeetupId(String meetupId) {

		logger.info("get meetup provision: {}", meetupId);

		Mono<Long> countConfirmedPeople = userMeetupService.getCountConfirmedPeopleByMeetupId(meetupId);

		Mono<Meetup> meetupMono = getById(meetupId);

		Mono<ClimateDataDto> climateMono = getClimateByMeetupId(meetupId);

		return Mono.zip(meetupMono, climateMono, countConfirmedPeople)
				.flatMap(t -> boxCalculator(t.getT3(), t.getT1(), t.getT2()));
	}

	private Mono<MeetupProvisionDto> boxCalculator(Long countConfirmedPeople, Meetup meetup, ClimateDataDto climate) {

		MeetupProvisionDto meetupProvisionDto = new MeetupProvisionDto();

		meetupProvisionDto.setNameMeetup(meetup.getName());

		meetupProvisionDto.setPeopleAmount(countConfirmedPeople);

		meetupProvisionDto.setTemp(climate.getMaxTemp());

		KieSession kieSession = kieContainer.newKieSession();
		kieSession.setGlobal("meetupProvisionDto", meetupProvisionDto);
		kieSession.insert(meetupProvisionDto);
		kieSession.fireAllRules();
		kieSession.dispose();

		return Mono.just(meetupProvisionDto);
	}

	@Override
	public Flux<Meetup> getByNotifiedIsFalse() {
		return meetupRepository.findByNotifiedIsFalseOrNotifiedIsNull();
	}

}
