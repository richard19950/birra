package com.santander.birra.service;

import com.santander.birra.dto.MeetupProvisionDto;
import com.santander.birra.dto.climate.ClimateDataDto;
import com.santander.birra.model.Meetup;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MeetupService {

	/**
	 * save the {@link Meetup} in the db
	 * 
	 * @param meetup {@link Mono} {@link Meetup}
	 * @return {@link Mono} {@link Meetup}
	 */
	public Mono<Meetup> save(Mono<Meetup> meetup);

	/**
	 * get the {@link Meetup} by the id if it doesn't exist it returns an
	 * {@link Mono#error(java.util.function.Supplier)}
	 * 
	 * @param meetupId {@link String}
	 * @return {@link Mono} {@link Meetup}
	 */
	public Mono<Meetup> getById(String meetupId);

	/**
	 * get the {@link Meetup} climate if you can't find it, returns an
	 * {@link Mono#error(java.util.function.Supplier)}
	 * 
	 * @param meetupId {@link String }
	 * @return {@link Mono} {@link ClimateDataDto}
	 */
	public Mono<ClimateDataDto> getClimateByMeetupId(String meetupId);

	/**
	 * gets the {@link MeetupProvisionDto} of {@link Meetup} obtained by a business
	 * rule in a drools file
	 * 
	 * @param meetupId {@link String}
	 * @return {@link Mono} {@link MeetupProvisionDto}
	 */
	public Mono<MeetupProvisionDto> getMeetupProvisionDtoByMeetupId(String meetupId);

	/**
	 * get the meetings that were not notified
	 * 
	 * @return {@link Mono} {@link Meetup}
	 */
	public Flux<Meetup> getByNotifiedIsFalse();

}
