package com.santander.birra.service;

import com.santander.birra.model.Province;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProvinceService {

	/**
	 * Get All {@link Province}
	 * 
	 * @return {@link Flux} {@link Province}
	 */
	public Flux<Province> getAll();

	/**
	 * Save {@link Province} in db
	 * 
	 * @param province {@link Mono} {@link Province}
	 * @return {@link Mono} {@link Province}
	 */
	public Mono<Province> save(Mono<Province> province);

	/**
	 * get the {@link Province} by id if it does not exist returns an
	 * {@link Mono#error(Throwable)}
	 * 
	 * @param provinceId {@link String}
	 * @return {@link Mono} {@link Province}
	 */
	public Mono<Province> getById(String provinceId);

}
