package com.santander.birra.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.santander.birra.model.User;
import com.santander.birra.repository.UserRepository;
import com.santander.birra.service.UserService;

import reactor.core.publisher.Mono;

@Service
public class UserServiceImpl implements UserService {

	Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Override
	public Mono<User> save(Mono<User> user) {

		logger.info("save user");

		return userRepository.saveAll(user).last();
	}

	@Override
	public Mono<User> getByUsername(String userName) {

		logger.info("get user: {}", userName);

		return userRepository.findByUserName(userName);
	}

}
