package com.santander.birra.service;

import com.santander.birra.model.User;

import reactor.core.publisher.Mono;

public interface UserService {

	Mono<User> save(Mono<User> user);

	Mono<User> getByUsername(String userName);
}
