package com.santander.birra.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.santander.birra.exception.ErrorConstant;
import com.santander.birra.exception.ServiceException;
import com.santander.birra.model.Province;
import com.santander.birra.repository.ProvinceRepository;
import com.santander.birra.service.ProvinceService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ProvinceServiceImpl implements ProvinceService {

	Logger logger = LoggerFactory.getLogger(ProvinceServiceImpl.class);

	@Autowired
	private ProvinceRepository provinceRepository;

	@Override
	public Flux<Province> getAll() {
		return provinceRepository.findAll();
	}

	@Override
	public Mono<Province> save(Mono<Province> province) {

		logger.info("save province");

		return provinceRepository.saveAll(province).last();
	}

	@Override
	public Mono<Province> getById(String provinceId) {

		logger.info("get province: {}", provinceId);

		return provinceRepository.findById(provinceId).switchIfEmpty(
				Mono.error(new ServiceException(ErrorConstant.PROVINCE_NOT_FOUND, HttpStatus.NOT_FOUND)));
	}

}
