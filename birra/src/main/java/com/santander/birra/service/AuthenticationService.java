package com.santander.birra.service;

import com.santander.birra.dto.auth.AuthRequestDto;
import com.santander.birra.dto.auth.AuthResponseDto;

import reactor.core.publisher.Mono;

public interface AuthenticationService {

	/**
	 * Authentic the user if everything is ok returns {@link AuthResponseDto} if it
	 * does not return a {@link Mono#error(java.util.function.Supplier)}
	 * 
	 * @param authRequestDto {@link AuthRequestDto}
	 * @return {@link Mono} {@link AuthResponseDto}
	 */
	Mono<AuthResponseDto> auth(AuthRequestDto authRequestDto);

}
