package com.santander.birra.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.santander.birra.exception.ErrorConstant;
import com.santander.birra.exception.ServiceException;
import com.santander.birra.model.UserMeetup;
import com.santander.birra.repository.UserMeetupRepository;
import com.santander.birra.service.UserMeetupService;

import reactor.core.publisher.Mono;

@Service
public class UserMeetupServiceImpl implements UserMeetupService {

	Logger logger = LoggerFactory.getLogger(UserMeetupServiceImpl.class);

	@Autowired
	private UserMeetupRepository userMeetupRepository;

	@Override
	public Mono<Long> getCountConfirmedPeopleByMeetupId(String meetupId) {

		logger.info("count confirmed people of the meetup: {} ", meetupId);

		return userMeetupRepository.countByMeetupIdAndToGoTrue(meetupId).switchIfEmpty(Mono.just(0L));
	}

	@Override
	public Mono<UserMeetup> save(Mono<UserMeetup> userMeetupMono) {

		logger.info("save userMeetup");

		Mono<UserMeetup> userMeetupValidMono = userMeetupMono
				.filterWhen(u -> userMeetupRepository
						.existsByMeetupIdAndUserId(u.getMeetup().getId(), u.getUser().getId()).map(b -> !b))
				.switchIfEmpty(Mono
						.error(new ServiceException(ErrorConstant.CONFIRMATION_ALREADY_EXISTS, HttpStatus.CONFLICT)));

		return userMeetupRepository.saveAll(userMeetupValidMono).last();
	}

}
