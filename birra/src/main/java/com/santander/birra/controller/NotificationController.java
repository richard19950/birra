package com.santander.birra.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.santander.birra.dto.notification.NotificationDto;

import io.swagger.v3.oas.annotations.Operation;
import reactor.core.publisher.Flux;

@RestController
public class NotificationController {

	Logger logger = LoggerFactory.getLogger(NotificationController.class);

	@Autowired
	private Flux<NotificationDto> flux;

	@Operation(summary = "Get notification by provinceId")
	@GetMapping(value = "/notification", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public Flux<NotificationDto> getNotification(@RequestParam String provinceId) {

		logger.info("open notification");

		return flux.filter(f -> f.getProvinceId().equals(provinceId));
	}

}
