package com.santander.birra.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.santander.birra.dto.ProvinceDto;
import com.santander.birra.mapper.ProvinceMapper;
import com.santander.birra.service.ProvinceService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/province")
@SecurityRequirement(name = "bearerAuth")
public class ProvinceController {

	Logger logger = LoggerFactory.getLogger(ProvinceController.class);

	@Autowired
	private ProvinceService provinceService;

	@Autowired
	private ProvinceMapper provinceMapper;

	@Operation(summary = "Save province")
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public Mono<ProvinceDto> save(@RequestBody @Valid ProvinceDto provinceDto) {

		logger.info("saving province: {}", provinceDto.getName());

		return provinceService.save(provinceMapper.convertToEntity(provinceDto))
				.flatMap(p -> provinceMapper.convertToDto(p));
	}

	@Operation(summary = "Get all province")
	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public Flux<ProvinceDto> getAll() {

		logger.info("get all provinces");

		return provinceService.getAll().flatMap(p -> provinceMapper.convertToDto(p));
	}

}
