package com.santander.birra.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.santander.birra.dto.auth.AuthRequestDto;
import com.santander.birra.dto.auth.AuthResponseDto;
import com.santander.birra.dto.error.ErrorResponse;
import com.santander.birra.service.AuthenticationService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import reactor.core.publisher.Mono;

@RestController
public class AuthenticationController {

	Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

	@Autowired
	private AuthenticationService authenticationService;

	@Operation(summary = "Authentic user")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK", content = @Content(schema = @Schema(implementation = AuthResponseDto.class))),
			@ApiResponse(responseCode = "401", description = "User or pass invalid", content = @Content(schema = @Schema(implementation = ErrorResponse.class))) })
	@PostMapping("/login")
	@ResponseStatus(value = HttpStatus.OK)
	public Mono<AuthResponseDto> login(@RequestBody @Valid AuthRequestDto auth) {

		logger.info("user entering: {}", auth.getUser());

		return authenticationService.auth(auth);
	}

}
