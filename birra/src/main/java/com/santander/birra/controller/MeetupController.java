package com.santander.birra.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.santander.birra.dto.MeetupDto;
import com.santander.birra.dto.MeetupProvisionDto;
import com.santander.birra.dto.UserMeetupDto;
import com.santander.birra.dto.climate.ClimateDataDto;
import com.santander.birra.mapper.MeetupMapper;
import com.santander.birra.mapper.UserMeetupMapper;
import com.santander.birra.service.MeetupService;
import com.santander.birra.service.UserMeetupService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/meetup")
@SecurityRequirement(name = "bearerAuth")
public class MeetupController {

	Logger logger = LoggerFactory.getLogger(MeetupController.class);

	@Autowired
	private MeetupService meetupService;

	@Autowired
	private MeetupMapper meetupMapper;

	@Autowired
	private UserMeetupService userMeetupService;

	@Autowired
	private UserMeetupMapper userMeetupMapper;

	@Operation(summary = "Save meetup")
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public Mono<MeetupDto> save(@RequestBody @Valid MeetupDto meetupDto) {

		logger.info("saving Meetup: {}", meetupDto.getName());

		return meetupService.save(meetupMapper.convertToEntity(meetupDto)).flatMap(m -> meetupMapper.convertToDto(m));
	}

	@Operation(summary = "Get climate by meetupId")
	@GetMapping("/climate/{meetup_id}")
	@ResponseStatus(value = HttpStatus.OK)
	public Mono<ClimateDataDto> getClimateByMeetupId(@PathVariable(name = "meetup_id") String meetupId) {

		logger.info("get the meetup climate: {}", meetupId);

		return meetupService.getClimateByMeetupId(meetupId);
	}

	@Operation(summary = "Get provision by meetupId")
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/provision/{meetup_id}")
	@ResponseStatus(value = HttpStatus.OK)
	public Mono<MeetupProvisionDto> getMeetupProvisionDtoByMeetupId(@PathVariable("meetup_id") String meetupId) {

		logger.info("get the meetup provision: {}", meetupId);

		return meetupService.getMeetupProvisionDtoByMeetupId(meetupId);
	}

	@Operation(summary = "save assistance")
	@PostMapping("/assistance")
	@ResponseStatus(value = HttpStatus.CREATED)
	public Mono<UserMeetupDto> confirmAssistance(@RequestBody @Valid UserMeetupDto userMeetupDto) {

		logger.info("confirming meeting attendance: {}", userMeetupDto.getMeetupId());

		return userMeetupService.save(userMeetupMapper.convertToEntity(userMeetupDto))
				.flatMap(u -> userMeetupMapper.convertToDto(u));
	}

}
