package com.santander.birra.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.santander.birra.dto.UserDto;
import com.santander.birra.model.User;

import reactor.core.publisher.Mono;

@Component
public class UserMapper {

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private PasswordEncoder passwordEncoder;

	/**
	 * Convert dto a to entity
	 * 
	 * @param meetupDto {@link UserDto}
	 * @return {@link Mono} {@link User}
	 */
	public Mono<User> convertToEntity(UserDto userDto) {

		userDto.setPass(passwordEncoder.encode(userDto.getPass()));

		return Mono.just(mapper.map(userDto, User.class));
	}

	/**
	 * Convert entity a to entity dto
	 * 
	 * @param meetup {@link User}
	 * @return {@link Mono} {@link UserDto}
	 */
	public Mono<UserDto> convertToDto(User user) {

		return Mono.just(mapper.map(user, UserDto.class));
	}
}
