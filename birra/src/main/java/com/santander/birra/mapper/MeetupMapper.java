package com.santander.birra.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.stereotype.Component;

import com.santander.birra.dto.MeetupDto;
import com.santander.birra.model.Meetup;
import com.santander.birra.model.Province;
import com.santander.birra.model.User;
import com.santander.birra.service.ProvinceService;

import reactor.core.publisher.Mono;

@Component
public class MeetupMapper {

	@Autowired
	private ProvinceService provinceService;

	@Autowired
	private ModelMapper mapper;

	/**
	 * Convert dto a to entity
	 * 
	 * @param meetupDto {@link MeetupDto}
	 * @return {@link Mono} {@link Meetup}
	 */
	public Mono<Meetup> convertToEntity(MeetupDto meetupDto) {

		Mono<Province> provinceMono = provinceService.getById(meetupDto.getProvinceId());

		return Mono.zip(provinceMono, ReactiveSecurityContextHolder.getContext()).flatMap(t -> {

			Meetup meetup = mapper.map(meetupDto, Meetup.class);

			meetup.setProvince(t.getT1());

			User user = mapper.map(t.getT2().getAuthentication().getPrincipal(), User.class);

			meetup.setOwner(user);

			return Mono.just(meetup);

		});

	}

	/**
	 * Convert entity a to entity dto
	 * 
	 * @param meetup {@link Meetup}
	 * @return {@link Mono} {@link MeetupDto}
	 */
	public Mono<MeetupDto> convertToDto(Meetup meetup) {

		MeetupDto meetupDto = mapper.map(meetup, MeetupDto.class);

		if (meetup.getProvince() != null)
			meetupDto.setProvinceId(meetup.getProvince().getId());

		return Mono.just(meetupDto);
	}

}
