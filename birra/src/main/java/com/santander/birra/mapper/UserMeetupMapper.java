package com.santander.birra.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.stereotype.Component;

import com.santander.birra.dto.UserDto;
import com.santander.birra.dto.UserMeetupDto;
import com.santander.birra.model.Meetup;
import com.santander.birra.model.User;
import com.santander.birra.model.UserMeetup;
import com.santander.birra.service.MeetupService;

import reactor.core.publisher.Mono;

@Component
public class UserMeetupMapper {

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private MeetupService meetupService;

	/**
	 * Convert dto a to entity
	 * 
	 * @param meetupDto {@link UserMeetupDto}
	 * @return {@link Mono} {@link UserMeetup}
	 */
	public Mono<UserMeetup> convertToEntity(UserMeetupDto userMeetupDto) {

		Mono<Meetup> meetupMono = meetupService.getById(userMeetupDto.getMeetupId());

		return Mono.zip(meetupMono, ReactiveSecurityContextHolder.getContext()).map(t -> {

			User user = mapper.map(t.getT2().getAuthentication().getPrincipal(), User.class);

			UserMeetup userMeetup = mapper.map(userMeetupDto, UserMeetup.class);

			userMeetup.setMeetup(t.getT1());

			userMeetup.setUser(user);

			return userMeetup;
		});

	}

	/**
	 * Convert entity a to entity dto
	 * 
	 * @param meetup {@link UserMeetup}
	 * @return {@link Mono} {@link UserMeetupDto}
	 */
	public Mono<UserMeetupDto> convertToDto(UserMeetup userMeetup) {

		return Mono.just(mapper.map(userMeetup, UserMeetupDto.class));
	}

}
