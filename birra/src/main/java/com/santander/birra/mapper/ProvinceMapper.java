package com.santander.birra.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.santander.birra.dto.MeetupDto;
import com.santander.birra.dto.ProvinceDto;
import com.santander.birra.model.Meetup;
import com.santander.birra.model.Province;

import reactor.core.publisher.Mono;

@Component
public class ProvinceMapper {

	@Autowired
	private ModelMapper mapper;

	/**
	 * Convert dto a to entity
	 * 
	 * @param meetupDto {@link ProvinceDto}
	 * @return {@link Mono} {@link Province}
	 */
	public Mono<Province> convertToEntity(ProvinceDto provinceDto) {

		return Mono.just(mapper.map(provinceDto, Province.class));
	}

	/**
	 * Convert entity a to entity dto
	 * 
	 * @param meetup {@link Province}
	 * @return {@link Mono} {@link ProvinceDto}
	 */
	public Mono<ProvinceDto> convertToDto(Province province) {

		return Mono.just(mapper.map(province, ProvinceDto.class));
	}
}
