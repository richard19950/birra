package com.santander.birra.init;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.santander.birra.model.Province;
import com.santander.birra.model.User;
import com.santander.birra.model.enums.Roles;
import com.santander.birra.repository.ProvinceRepository;
import com.santander.birra.repository.UserRepository;

@Component
public class InitApp {

	Logger logger = LoggerFactory.getLogger(InitApp.class);

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProvinceRepository provinceRepository;

	/**
	 * Start some data in the db for testing
	 */
	@PostConstruct
	public void startApp() {

		logger.info("init app");

		User userAdmin = new User();

		String userNameAdmin = "admin";

		userAdmin.setRole(Roles.ROLE_ADMIN);
		userAdmin.setUserName(userNameAdmin);
		userAdmin.setPass(passwordEncoder.encode("1234"));

		userRepository.findByUserName(userNameAdmin).switchIfEmpty(userRepository.save(userAdmin))
				.subscribe(u -> logger.info("persistent user: {}", u.getUserName()));

		User user = new User();

		String userName = "user";

		user.setRole(Roles.ROLE_USER);
		user.setUserName(userName);
		user.setPass(passwordEncoder.encode("1234"));

		userRepository.findByUserName(userName).switchIfEmpty(userRepository.save(user))
				.subscribe(u -> logger.info("persistent user: {}", u.getUserName()));

		Province province = new Province();

		String nameProvince = "Mendoza";

		province.setName(nameProvince);
		province.setDescription(nameProvince);

		provinceRepository.findByName(nameProvince).switchIfEmpty(provinceRepository.save(province))
				.subscribe(p -> logger.info("persistent province: {}", p.getName()));

	}

}
