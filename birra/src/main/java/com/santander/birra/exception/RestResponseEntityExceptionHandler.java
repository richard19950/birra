package com.santander.birra.exception;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.support.WebExchangeBindException;

import com.santander.birra.dto.error.Error;
import com.santander.birra.dto.error.ErrorResponse;

import reactor.core.publisher.Mono;

@ControllerAdvice
@ResponseBody
public class RestResponseEntityExceptionHandler {

	Logger logger = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

	/**
	 * Returns type errors in a standard format
	 * {@link MethodArgumentNotValidException}
	 * 
	 * @param ex {@link WebExchangeBindException}
	 * @return {@link Mono} {@link ErrorResponse}
	 */
	@ExceptionHandler(value = { WebExchangeBindException.class })
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	protected Mono<ErrorResponse> handleValid(WebExchangeBindException ex) {

		List<Error> errors = ex.getFieldErrors().stream().map(f -> new Error(f.getDefaultMessage()))
				.collect(Collectors.toList());

		logger.error("Input parameter error", ex);

		return Mono.just(new ErrorResponse(errors));
	}

	/**
	 * Returns the different service errors in a standard format
	 * 
	 * @param ex {@link ServiceException}
	 * @return {@link Mono} {@link ErrorResponse}
	 */
	@ExceptionHandler(value = ServiceException.class)
	protected Mono<ResponseEntity<ErrorResponse>> handleServiceEception(ServiceException ex) {

		Error error = new Error(ex.getMessage());

		logger.error("Business logic error", ex);

		return Mono.just(new ResponseEntity<>(new ErrorResponse(List.of(error)), ex.getCode()));
	}

	/**
	 * Returns access denied errors in standard format
	 * 
	 * @param ex {@link AccessDeniedException}
	 * @return {@link ResponseEntity}
	 */
	@ExceptionHandler(value = AccessDeniedException.class)
	protected Mono<ResponseEntity<ErrorResponse>> handleServiceEception(AccessDeniedException ex) {

		Error error = new Error(ErrorConstant.INSUFFICIENT_PRIVILEGES);

		logger.error("Insufficient privileges error", ex);

		return Mono.just(new ResponseEntity<>(new ErrorResponse(List.of(error)), HttpStatus.FORBIDDEN));
	}

	/**
	 * Returns unforeseen errors in a standard format
	 * 
	 * @param ex {@link Exception}
	 * @return {@link ResponseEntity}
	 */
	@ExceptionHandler(value = Exception.class)
	protected Mono<ResponseEntity<ErrorResponse>> handleServiceEception(Exception ex) {

		Error error = new Error(ex.getMessage());

		logger.error("Unknown error", ex);

		return Mono.just(new ResponseEntity<>(new ErrorResponse(List.of(error)), HttpStatus.INTERNAL_SERVER_ERROR));
	}

}
