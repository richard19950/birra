package com.santander.birra.exception;

/**
 * 
 * Class of constants with possible errors in the api
 * 
 * @author ricky
 *
 */
public class ErrorConstant {

	private ErrorConstant() {
	}

	public static final String MEETAUP_NOT_FOUND = "Meetaup not found";

	public static final String THE_MEETUP_IS_OLD = "The meetup is old";

	public static final String CLIMATE_NOT_FOUND = "The climate calculation is not yet available for the day of the meetup";

	public static final String UNEXPECTED_ERROR = "UNEXPECTED ERROR";

	public static final String PROVINCE_NOT_FOUND = "province not found";

	public static final String INCORRECT_USER_OR_PASSWORD = "Incorrect user or password";

	public static final String INCORRECT_OR_EXPIRED_TOKEN = "Incorrect or expired token";

	public static final String INSUFFICIENT_PRIVILEGES = "Insufficient privileges";

	public static final String CONFIRMATION_ALREADY_EXISTS = "Confirmation already exists for this user";

}
