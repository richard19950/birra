package com.santander.birra.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class UserMeetup {

	@Id
	private String id;

	private User user;

	private Meetup meetup;

	private Boolean toGo;

	private Boolean went;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Meetup getMeetup() {
		return meetup;
	}

	public void setMeetup(Meetup meetup) {
		this.meetup = meetup;
	}

	public Boolean getToGo() {
		return toGo;
	}

	public void setToGo(Boolean toGo) {
		this.toGo = toGo;
	}

	public Boolean getWent() {
		return went;
	}

	public void setWent(Boolean went) {
		this.went = went;
	}

}
