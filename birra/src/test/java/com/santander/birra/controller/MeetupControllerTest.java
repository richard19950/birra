package com.santander.birra.controller;

import static org.mockito.Mockito.when;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.santander.birra.dto.MeetupDto;
import com.santander.birra.dto.UserMeetupDto;
import com.santander.birra.mapper.MeetupMapper;
import com.santander.birra.mapper.UserMeetupMapper;
import com.santander.birra.model.Meetup;
import com.santander.birra.model.UserMeetup;
import com.santander.birra.security.AuthenticationManager;
import com.santander.birra.security.JWTUtil;
import com.santander.birra.security.SecurityContextRepository;
import com.santander.birra.service.MeetupService;
import com.santander.birra.service.UserMeetupService;

import reactor.core.publisher.Mono;

@AutoConfigureWebTestClient
@RunWith(SpringJUnit4ClassRunner.class)
@WebFluxTest(controllers = { MeetupController.class })
@Import({ AuthenticationManager.class, JWTUtil.class, SecurityContextRepository.class })
class MeetupControllerTest {

	@Autowired
	WebTestClient webTestClient;

	@MockBean
	private MeetupService meetupService;

	@MockBean
	private MeetupMapper meetupMapper;

	@MockBean
	private UserMeetupService userMeetupService;

	@MockBean
	private UserMeetupMapper userMeetupMapper;

	@Test
	@WithMockUser(username = "admin", authorities = { "ROLE_ADMIN" }, password = "1234")
	void save_theReturnOk() {

		MeetupDto meetupDto = getMeetupDto();

		Meetup meetup = getMeetup();

		Mono<Meetup> meetupMono = Mono.just(meetup);

		when(meetupMapper.convertToEntity(meetupDto)).thenReturn(meetupMono);

		when(meetupService.save(Mockito.any())).thenReturn(meetupMono);

		when(meetupMapper.convertToDto(meetup)).thenReturn(Mono.just(meetupDto));

		webTestClient.post().uri("/meetup").bodyValue(meetupDto).exchange().expectStatus().isCreated();
	}

	@Test
	@WithMockUser(username = "admin", authorities = { "ROLE_ADMIN" }, password = "1234")
	void save_theReturnBadRequest() {

		MeetupDto meetupDto = getMeetupDto();

		meetupDto.setDate(null);

		webTestClient.post().uri("/meetup").bodyValue(meetupDto).exchange().expectStatus().isBadRequest();
	}

	@Test
	@WithMockUser(username = "admin", authorities = { "ROLE_ADMIN" }, password = "1234")
	void getClimateByMeetupId_theReturnOk() {

		String meetupId = "12668";

		when(meetupService.getClimateByMeetupId(meetupId)).thenReturn(Mono.empty());

		webTestClient.get().uri("/meetup/climate/" + meetupId).exchange().expectStatus().isOk();
	}

	@Test
	@WithMockUser(username = "admin", authorities = { "ROLE_ADMIN" }, password = "1234")
	void getMeetupProvisionDtoByMeetupId_theReturnOk() {

		String meetupId = "12668";

		when(meetupService.getMeetupProvisionDtoByMeetupId(meetupId)).thenReturn(Mono.empty());

		webTestClient.get().uri("/meetup/provision/" + meetupId).exchange().expectStatus().isOk();
	}

	@Test
	@WithMockUser(username = "admin", authorities = { "ROLE_ADMIN" }, password = "1234")
	void confirmAssistance_theRetuenok() {

		UserMeetupDto userMeetupDto = getUserMeetupDto();

		UserMeetup userMeetup = getUserMeetup();

		Mono<UserMeetup> userMeetupMono = Mono.just(userMeetup);

		when(userMeetupMapper.convertToEntity(userMeetupDto)).thenReturn(userMeetupMono);

		when(userMeetupService.save(Mockito.any())).thenReturn(userMeetupMono);

		when(userMeetupMapper.convertToDto(userMeetup)).thenReturn(Mono.just(userMeetupDto));

		webTestClient.post().uri("/meetup/assistance").bodyValue(userMeetupDto).exchange().expectStatus().isCreated();
	}
	
	@Test
	@WithMockUser(username = "admin", authorities = { "ROLE_ADMIN" }, password = "1234")
	void confirmAssistance_theReturnBadRequest() {

		UserMeetupDto userMeetupDto = getUserMeetupDto();

		userMeetupDto.setMeetupId(null);
		
		webTestClient.post().uri("/meetup/assistance").bodyValue(userMeetupDto).exchange().expectStatus().isBadRequest();
	}

	private MeetupDto getMeetupDto() {

		MeetupDto meetupDto = new MeetupDto();

		meetupDto.setDate(LocalDate.now());
		meetupDto.setName("juntada");
		meetupDto.setProvinceId("1587");

		return meetupDto;
	}

	private Meetup getMeetup() {

		Meetup meetup = new Meetup();

		meetup.setDate(LocalDate.now());
		meetup.setName("juntada");
		meetup.setAddress("asdsa");

		return meetup;
	}

	private UserMeetupDto getUserMeetupDto() {

		UserMeetupDto userMeetupDto = new UserMeetupDto();

		userMeetupDto.setMeetupId("4225");
		userMeetupDto.setToGo(true);

		return userMeetupDto;
	}

	private UserMeetup getUserMeetup() {

		UserMeetup userMeetup = new UserMeetup();

		userMeetup.setId("5205");
		userMeetup.setToGo(true);
		userMeetup.setMeetup(getMeetup());

		return userMeetup;
	}
}
