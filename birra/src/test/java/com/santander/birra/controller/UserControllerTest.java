package com.santander.birra.controller;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.santander.birra.dto.UserDto;
import com.santander.birra.mapper.UserMapper;
import com.santander.birra.model.User;
import com.santander.birra.model.enums.Roles;
import com.santander.birra.security.AuthenticationManager;
import com.santander.birra.security.JWTUtil;
import com.santander.birra.security.SecurityContextRepository;
import com.santander.birra.service.UserService;

import reactor.core.publisher.Mono;

@AutoConfigureWebTestClient
@RunWith(SpringJUnit4ClassRunner.class)
@WebFluxTest(controllers = { UserController.class })
@Import({ AuthenticationManager.class, JWTUtil.class, SecurityContextRepository.class })
class UserControllerTest {

	@MockBean
	private UserService userService;

	@MockBean
	private UserMapper userMapper;

	@Autowired
	WebTestClient webTestClient;

	@Test
	@WithMockUser(username = "admin", authorities = { "ROLE_ADMIN" }, password = "1234")
	void save_theReturnOk() throws Exception {

		UserDto userDto = getUserDto();

		User user = getUser();

		Mono<User> userMono = Mono.just(user);

		when(userMapper.convertToEntity(userDto)).thenReturn(userMono);

		when(userService.save(Mockito.any())).thenReturn(userMono);

		when(userMapper.convertToDto(user)).thenReturn(Mono.just(userDto));

		webTestClient.post().uri("/user").bodyValue(userDto).exchange().expectStatus().isCreated();

	}

	@Test
	@WithMockUser(username = "admin", authorities = { "ROLE_ADMIN" }, password = "1234")
	void save_theReturnBadRequest() throws Exception {

		UserDto userDto = getUserDto();

		userDto.setUserName(null);

		webTestClient.post().uri("/user").bodyValue(userDto).exchange().expectStatus().isBadRequest();

	}

	private UserDto getUserDto() {

		UserDto userDto = new UserDto();
		userDto.setUserName("user");
		userDto.setPass("1234");
		userDto.setRole(Roles.ROLE_ADMIN);

		return userDto;
	}

	private User getUser() {

		User user = new User();

		user.setPass("1234");
		user.setUserName("user");
		user.setId("123");
		user.setRole(Roles.ROLE_ADMIN);

		return user;
	}

}
