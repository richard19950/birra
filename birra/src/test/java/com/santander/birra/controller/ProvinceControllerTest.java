package com.santander.birra.controller;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.santander.birra.dto.ProvinceDto;
import com.santander.birra.mapper.ProvinceMapper;
import com.santander.birra.model.Province;
import com.santander.birra.security.AuthenticationManager;
import com.santander.birra.security.JWTUtil;
import com.santander.birra.security.SecurityContextRepository;
import com.santander.birra.service.ProvinceService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@AutoConfigureWebTestClient
@RunWith(SpringJUnit4ClassRunner.class)
@WebFluxTest(controllers = { ProvinceController.class })
@Import({ AuthenticationManager.class, JWTUtil.class, SecurityContextRepository.class })
class ProvinceControllerTest {

	@MockBean
	private ProvinceService provinceService;

	@MockBean
	private ProvinceMapper provinceMapper;

	@Autowired
	WebTestClient webTestClient;

	@Test
	@WithMockUser(username = "admin", authorities = { "ROLE_ADMIN" }, password = "1234")
	void save_theReturnOk() {

		ProvinceDto provinceDto = getProvinceDto();

		Province province = getProvince();

		Mono<Province> provinceMono = Mono.just(province);

		when(provinceMapper.convertToEntity(provinceDto)).thenReturn(provinceMono);

		when(provinceService.save(Mockito.any())).thenReturn(provinceMono);

		when(provinceMapper.convertToDto(province)).thenReturn(Mono.just(provinceDto));

		webTestClient.post().uri("/province").bodyValue(provinceDto).exchange().expectStatus().isCreated();
	}

	@Test
	@WithMockUser(username = "admin", authorities = { "ROLE_ADMIN" }, password = "1234")
	void save_theReturnBadRequest() {

		ProvinceDto provinceDto = getProvinceDto();

		provinceDto.setName(null);

		webTestClient.post().uri("/province").bodyValue(provinceDto).exchange().expectStatus().isBadRequest();
	}

	@Test
	@WithMockUser(username = "admin", authorities = { "ROLE_ADMIN" }, password = "1234")
	void getAll_theReturnOk() {

		Province province = getProvince();

		ProvinceDto provinceDto = getProvinceDto();

		when(provinceService.getAll()).thenReturn(Flux.just(province));

		when(provinceMapper.convertToDto(province)).thenReturn(Mono.just(provinceDto));

		webTestClient.get().uri("/province").exchange().expectStatus().isOk();
	}

	private ProvinceDto getProvinceDto() {

		ProvinceDto provinceDto = new ProvinceDto();

		provinceDto.setName("Mendoza");
		provinceDto.setDescription("Mendoza");

		return provinceDto;
	}

	private Province getProvince() {

		Province province = new Province();

		province.setName("Mendoza");
		province.setDescription("Mendoza");
		province.setId("102");

		return province;
	}

}
