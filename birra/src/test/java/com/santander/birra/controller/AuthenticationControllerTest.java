package com.santander.birra.controller;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.santander.birra.dto.auth.AuthRequestDto;
import com.santander.birra.security.AuthenticationManager;
import com.santander.birra.security.JWTUtil;
import com.santander.birra.security.SecurityContextRepository;
import com.santander.birra.service.AuthenticationService;

import reactor.core.publisher.Mono;

@AutoConfigureWebTestClient
@RunWith(SpringJUnit4ClassRunner.class)
@WebFluxTest(controllers = { AuthenticationController.class })
@Import({ AuthenticationManager.class, JWTUtil.class, SecurityContextRepository.class })
class AuthenticationControllerTest {

	@Autowired
	WebTestClient webTestClient;

	@MockBean
	private AuthenticationService authenticationService;

	@Test
	void login_theReturnOk() {

		AuthRequestDto authRequestDto = getAuthRequestDto();

		when(authenticationService.auth(authRequestDto)).thenReturn(Mono.empty());

		webTestClient.post().uri("/login").bodyValue(authRequestDto).exchange().expectStatus().isOk();
	}

	@Test
	void login_theReturnBadRequest() {

		AuthRequestDto authRequestDto = getAuthRequestDto();

		authRequestDto.setPass(null);

		when(authenticationService.auth(authRequestDto)).thenReturn(Mono.empty());

		webTestClient.post().uri("/login").bodyValue(authRequestDto).exchange().expectStatus().isBadRequest();
	}

	private AuthRequestDto getAuthRequestDto() {

		AuthRequestDto authRequestDto = new AuthRequestDto();

		authRequestDto.setPass("12345");
		authRequestDto.setUser("admin");

		return authRequestDto;
	}

}
