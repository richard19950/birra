package com.santander.birra.service;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.redis.core.ReactiveRedisTemplate;

import com.santander.birra.dto.climate.ClimateDataDto;
import com.santander.birra.dto.climate.ClimateResponseDto;
import com.santander.birra.exception.ServiceException;
import com.santander.birra.service.impl.ClimateServiceImpl;

import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@RunWith(MockitoJUnitRunner.class)
class ClimateServiceImplTest {

	@InjectMocks
	ClimateServiceImpl climateServiceImpl;

	@Mock
	private ReactiveRedisTemplate<String, ClimateResponseDto> redisTemplate;

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
	}

	static final String PROVINCE_ID = "102";

	@Test
	void getClimateByDateAndProvince_OldDate() {

		LocalDate now = LocalDate.now();

		LocalDate oldDate = now.minusDays(1);

		Mono<ClimateDataDto> climateMono = climateServiceImpl.getClimateByDateAndProvince(oldDate, PROVINCE_ID);

		StepVerifier.create(climateMono).expectErrorMatches(t -> t instanceof ServiceException).verify();
	}

}
