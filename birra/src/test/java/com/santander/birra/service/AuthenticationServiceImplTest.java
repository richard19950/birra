package com.santander.birra.service;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.santander.birra.dto.auth.AuthRequestDto;
import com.santander.birra.dto.auth.AuthResponseDto;
import com.santander.birra.exception.ServiceException;
import com.santander.birra.model.User;
import com.santander.birra.security.JWTUtil;
import com.santander.birra.service.impl.AuthenticationServiceImpl;

import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@RunWith(MockitoJUnitRunner.class)
class AuthenticationServiceImplTest {

	@InjectMocks
	AuthenticationServiceImpl authenticationServiceImpl;

	@Mock
	PasswordEncoder passwordEncoder;

	@Mock
	private UserService userService;

	@Mock
	private JWTUtil jwtUtil;

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void auth_userNotExiste() {

		AuthRequestDto authRequestDto = getAuthRequestDto();

		when(userService.getByUsername(authRequestDto.getUser())).thenReturn(Mono.empty());

		Mono<AuthResponseDto> authMono = authenticationServiceImpl.auth(authRequestDto);

		StepVerifier.create(authMono).expectErrorMatches(t -> t instanceof ServiceException).verify();
	}

	@Test
	void auth_userNotMatchPass() {

		AuthRequestDto authRequestDto = getAuthRequestDto();

		User user = getUser();

		when(userService.getByUsername(authRequestDto.getUser())).thenReturn(Mono.just(user));

		when(passwordEncoder.matches(authRequestDto.getPass(), user.getPass())).thenReturn(false);

		Mono<AuthResponseDto> authMono = authenticationServiceImpl.auth(authRequestDto);

		StepVerifier.create(authMono).expectErrorMatches(t -> t instanceof ServiceException).verify();
	}

	@Test
	void auth_theReturnOk() {

		AuthRequestDto authRequestDto = getAuthRequestDto();

		User user = getUser();

		when(userService.getByUsername(authRequestDto.getUser())).thenReturn(Mono.just(user));

		when(passwordEncoder.matches(authRequestDto.getPass(), user.getPass())).thenReturn(true);

		when(jwtUtil.generateToken(user)).thenReturn("1234");

		Mono<AuthResponseDto> authMono = authenticationServiceImpl.auth(authRequestDto);

		StepVerifier.create(authMono).expectNextMatches(a -> a.getToken().equals("1234")).expectComplete().verify();
	}

	private AuthRequestDto getAuthRequestDto() {

		AuthRequestDto authRequestDto = new AuthRequestDto();

		authRequestDto.setPass("12345");
		authRequestDto.setUser("admin");

		return authRequestDto;
	}

	private User getUser() {

		User user = new User();

		user.setPass("120");
		user.setUserName("admin");

		return user;
	}
}
