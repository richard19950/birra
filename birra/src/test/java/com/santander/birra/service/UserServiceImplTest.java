package com.santander.birra.service;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.santander.birra.model.User;
import com.santander.birra.model.enums.Roles;
import com.santander.birra.repository.UserRepository;
import com.santander.birra.service.impl.UserServiceImpl;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@RunWith(MockitoJUnitRunner.class)
class UserServiceImplTest {

	@InjectMocks
	UserServiceImpl userServiceImpl;

	@Mock
	private UserRepository userRepository;

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void save_theReturnOk() {

		User user = getUser();

		Mono<User> userMono = Mono.just(user);

		when(userRepository.saveAll(userMono)).thenReturn(Flux.just(user));

		Mono<User> userReturnMono = userServiceImpl.save(userMono);

		StepVerifier.create(userReturnMono).expectNextMatches(u -> u.getId().equals(user.getId())).expectComplete()
				.verify();
	}

	@Test
	void getByUsername_theReturnOk() {

		User user = getUser();

		when(userRepository.findByUserName(user.getUserName())).thenReturn(Mono.just(user));

		Mono<User> userMono = userServiceImpl.getByUsername(user.getUserName());

		StepVerifier.create(userMono).expectNextMatches(u -> u.getId().equals(user.getId())).expectComplete().verify();
	}

	private User getUser() {

		User user = new User();

		user.setId("123");
		user.setUserName("admin");
		user.setPass("1234");
		user.setRole(Roles.ROLE_ADMIN);

		return user;
	}

}
