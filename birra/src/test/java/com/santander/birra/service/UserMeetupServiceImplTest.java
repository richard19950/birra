package com.santander.birra.service;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.santander.birra.exception.ServiceException;
import com.santander.birra.model.Meetup;
import com.santander.birra.model.User;
import com.santander.birra.model.UserMeetup;
import com.santander.birra.repository.UserMeetupRepository;
import com.santander.birra.service.impl.UserMeetupServiceImpl;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@RunWith(MockitoJUnitRunner.class)
class UserMeetupServiceImplTest {

	@InjectMocks
	UserMeetupServiceImpl userMeetupServiceImpl;

	@Mock
	private UserMeetupRepository userMeetupRepository;

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
	}

	static final String MEETUP_ID = "1234";

	@Test
	void getCountConfirmedPeopleByMeetupId_theReturnOk() {

		when(userMeetupRepository.countByMeetupIdAndToGoTrue(MEETUP_ID)).thenReturn(Mono.just(2L));

		Mono<Long> countMono = userMeetupServiceImpl.getCountConfirmedPeopleByMeetupId(MEETUP_ID);

		StepVerifier.create(countMono).expectNextMatches(a -> a.equals(2L)).expectComplete().verify();
	}

	@Test
	void getCountConfirmedPeopleByMeetupId_theReturnCero() {

		when(userMeetupRepository.countByMeetupIdAndToGoTrue(MEETUP_ID)).thenReturn(Mono.empty());

		Mono<Long> countMono = userMeetupServiceImpl.getCountConfirmedPeopleByMeetupId(MEETUP_ID);

		StepVerifier.create(countMono).expectNextMatches(a -> a.equals(0L)).expectComplete().verify();
	}

	@SuppressWarnings("unchecked")
	@Test
	void save_theReturnOk() {

		UserMeetup userMeetup = getUserMeetup();

		Mono<UserMeetup> userMeetupMono = Mono.just(userMeetup);

		when(userMeetupRepository.existsByMeetupIdAndUserId(userMeetup.getMeetup().getId(),
				userMeetup.getUser().getId())).thenReturn(Mono.just(false));

		when(userMeetupRepository.saveAll((Mono<UserMeetup>) Mockito.any())).thenReturn(Flux.just(userMeetup));

		Mono<UserMeetup> userMeetupReturnMono = userMeetupServiceImpl.save(userMeetupMono);

		StepVerifier.create(userMeetupReturnMono).expectNextMatches(u -> u.getId().equals(userMeetup.getId()))
				.expectComplete().verify();

	}

	private UserMeetup getUserMeetup() {

		Meetup meetup = new Meetup();

		meetup.setId("123");

		User user = new User();

		user.setId("145");

		UserMeetup userMeetup = new UserMeetup();

		userMeetup.setId("1234");
		userMeetup.setMeetup(meetup);
		userMeetup.setToGo(true);
		userMeetup.setUser(user);

		return userMeetup;
	}

}
