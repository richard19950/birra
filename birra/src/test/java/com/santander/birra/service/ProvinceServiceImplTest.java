package com.santander.birra.service;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.santander.birra.exception.ServiceException;
import com.santander.birra.model.Province;
import com.santander.birra.repository.ProvinceRepository;
import com.santander.birra.service.impl.ProvinceServiceImpl;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@RunWith(MockitoJUnitRunner.class)
class ProvinceServiceImplTest {

	@InjectMocks
	ProvinceServiceImpl provinceServiceImpl;

	@Mock
	private ProvinceRepository provinceRepository;

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
	}

	static final String PROVINCE_ID = "1233";

	@Test
	void getAll_theReturnOk() {

		Mono<Province> provinceMono = Mono.just(getProvince());

		when(provinceRepository.saveAll(provinceMono)).thenReturn(Flux.just(getProvince()));

		Mono<Province> provinceReturnMono = provinceServiceImpl.save(provinceMono);

		StepVerifier.create(provinceReturnMono).expectNextMatches(p -> p.getId().equals(getProvince().getId()))
				.expectComplete().verify();

	}

	@Test
	void getById_theReturnOk() {

		Province province = getProvince();

		when(provinceRepository.findById(PROVINCE_ID)).thenReturn(Mono.just(province));

		Mono<Province> provinceMono = provinceServiceImpl.getById(PROVINCE_ID);

		StepVerifier.create(provinceMono).expectNextMatches(p -> p.getId().equals(province.getId())).expectComplete()
				.verify();

	}

	@Test
	void getById_theReturnError() {

		when(provinceRepository.findById(PROVINCE_ID)).thenReturn(Mono.empty());

		Mono<Province> provinceMono = provinceServiceImpl.getById(PROVINCE_ID);

		StepVerifier.create(provinceMono).expectErrorMatches(p -> p instanceof ServiceException).verify();
	}

	private Province getProvince() {

		Province province = new Province();

		province.setDescription("Mendoza");
		province.setName("Mendoa");
		province.setId("1233");

		return province;
	}

}
