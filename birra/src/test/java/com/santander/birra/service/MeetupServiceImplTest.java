package com.santander.birra.service;

import static org.mockito.Mockito.when;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.ObjectFactory;

import com.santander.birra.dto.climate.ClimateDataDto;
import com.santander.birra.exception.ServiceException;
import com.santander.birra.model.Meetup;
import com.santander.birra.model.Province;
import com.santander.birra.repository.MeetupRepository;
import com.santander.birra.service.impl.MeetupServiceImpl;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@RunWith(MockitoJUnitRunner.class)
class MeetupServiceImplTest {

	@InjectMocks
	MeetupServiceImpl meetupServiceImpl;

	@Mock
	private MeetupRepository meetupRepository;

	@Mock
	private ClimateService climateService;

	@Mock
	private UserMeetupService userMeetupService;

	@Mock
	private KieContainer kieContainer;

	@Mock
	private ObjectFactory<KieSession> kieSessionFactory;

	static final String MEETUP_ID = "1234";

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void save_theReturnOk() {

		Mono<Meetup> meetuoMono = Mono.just(getMeetup());

		when(meetupRepository.saveAll(meetuoMono)).thenReturn(Flux.just(getMeetup()));

		Mono<Meetup> meetupReturn = meetupServiceImpl.save(meetuoMono);

		StepVerifier.create(meetupReturn).expectNextMatches(m -> m.getId().equals("1234")).expectComplete().verify();
	}

	@Test
	void getById_theReturnOk() {

		Mono<Meetup> meetuoMono = Mono.just(getMeetup());

		when(meetupRepository.findById(MEETUP_ID)).thenReturn(meetuoMono);

		Mono<Meetup> meetupReturn = meetupServiceImpl.getById(MEETUP_ID);

		StepVerifier.create(meetupReturn).expectNextMatches(m -> m.getId().equals("1234")).expectComplete().verify();
	}

	@Test
	void getById_theReturnError() {

		when(meetupRepository.findById(MEETUP_ID)).thenReturn(Mono.empty());

		Mono<Meetup> meetupReturn = meetupServiceImpl.getById(MEETUP_ID);

		StepVerifier.create(meetupReturn).expectErrorMatches(t -> t instanceof ServiceException).verify();
	}

	@Test
	void getClimateByMeetupId_theReturnOK() {

		Meetup meetup = getMeetup();

		ClimateDataDto climateDataDto = getClimateDataDto();

		when(meetupRepository.findById(MEETUP_ID)).thenReturn(Mono.just(meetup));

		when(climateService.getClimateByDateAndProvince(meetup.getDate(), meetup.getProvince().getName()))
				.thenReturn(Mono.just(climateDataDto));

		Mono<ClimateDataDto> climateDataDtoMono = meetupServiceImpl.getClimateByMeetupId(MEETUP_ID);

		StepVerifier.create(climateDataDtoMono)
				.expectNextMatches(c -> c.getMaxTemp().equals(climateDataDto.getMaxTemp())).expectComplete().verify();

	}

	@Test
	void getClimateByMeetupId_theReturnError() {

		Meetup meetup = getMeetup();

		when(meetupRepository.findById(MEETUP_ID)).thenReturn(Mono.just(meetup));

		when(climateService.getClimateByDateAndProvince(meetup.getDate(), meetup.getProvince().getName()))
				.thenReturn(Mono.empty());

		Mono<ClimateDataDto> climateDataDtoMono = meetupServiceImpl.getClimateByMeetupId(MEETUP_ID);

		StepVerifier.create(climateDataDtoMono).expectErrorMatches(c -> c instanceof ServiceException).verify();

	}

	private Meetup getMeetup() {

		Meetup meetup = new Meetup();

		meetup.setDate(LocalDate.now());
		meetup.setName("juntada");
		meetup.setAddress("asdsa");
		meetup.setId("1234");

		Province province = new Province();

		province.setId("11");
		province.setName("Mendoza");

		meetup.setProvince(province);

		return meetup;
	}

	private ClimateDataDto getClimateDataDto() {

		ClimateDataDto climateDataDto = new ClimateDataDto();

		climateDataDto.setLowTemp(10D);
		climateDataDto.setMaxTemp(30D);
		climateDataDto.setValidDate(LocalDate.now());

		return climateDataDto;
	}

}
