# Birra App

### App to organize meetup without missing beer (the business rule of this app was made with the drools rule engine).

### The app when lifting for the first time creates a admin user and a normal user.

* Admin user:

    * user: admin
    * pass: 1234
* Normal user:

    * user: user
    * pass: 1234

#### The app has security by token jwt.

### The app generates a province when starting "Mendoza" since the climate cache (redis) caches the weather by provinces, More provinces can be generated with a post, but in the case of doing so, you will have to wait a few minutes for the cache to be updated or restart the app since the update of the cache is not developed when loading new provinces.

## Notification:

### the app has an endpoint based on SSE (server sent events) to send a notification to users when new meetups are generated


## deploy app

     1. install docker and docker-compose
     2. cd  ./birra 
     3. run command docker-componse up 

| the first time you deploy the app it will take a long time.




### Url swagger: 

http://localhost:8080/swagger-ui.html

## technologys:

 
 * Spring boot
 * Spring webFlux
 * Spring security reactive
 * Drools
 * MongoDB
 * Spring data reactive
 * redis
 * Docker
 * Docker compose 


